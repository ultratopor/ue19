﻿// 19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
class Animal
{
public:
   virtual void Voice()
   {
        std::cout << "voice";
   }
};
class Dog : public Animal
{
        void Voice() override
        {
            std::cout << "woof"<<std::endl;
        }
};
class Cat : public Animal
{
        void Voice() override
        {
            std::cout << "meow" << std::endl;
        }
};
class Duck : public Animal
{
        void Voice() override
        {
             std::cout << "quack" << std::endl;
        }
};

int main()
{
    Animal* dog=new Dog();
    Animal* cat=new Cat();
    Animal* duck=new Duck();
    Animal* animals[3] = { dog,cat,duck };
    for (size_t i = 0; i < 3; i++)
    {
        animals[i]->Voice();
    }
    delete dog, cat, duck,animals;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
